import React, { useState, useEffect } from 'react';
import { Doughnut } from 'react-chartjs-2';
import axios from 'axios';
import socketIOClient from 'socket.io-client'




const anF = async (frameworkName) => {

  return new Promise(async (resolve, reject) => {
    const result = await axios(
      'http://localhost:8082/vote/realtimevote/'+frameworkName,
    );
    resolve(result.data.val);
  });

}

async function setVal(frameworkName) {

  let dataval = await anF(frameworkName);

  const data = {
    labels: [
      'Angular',
      'React',
      'Vue'

    ],
    datasets: [{
      data: dataval,
      backgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56'
      ],
      hoverBackgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56'
      ]
    }]
  };
  return data
}





const Chart = () => {

  const [Valnew, setValnew] = useState({});
  const [endpoint] = useState('http://localhost:8082');


  async function fetchMyAPI() {
    const Val = await setVal("showinit");
    setValnew(Val);

  }
  useEffect(() => {
    fetchMyAPI();
    response();
  }, []);

   // รอรับข้อมูลเมื่อ server มีการ update
 const response = () => {
  const socket = socketIOClient(endpoint)
  socket.on('newVal', async (newVal) => {
    const Val = await setVal(newVal);
    setValnew(Val);
  })
}


  return (
    <div>
      <Doughnut
        data={Valnew}
        width={100}
        height={50}
      />
    </div>
  )
}

export default Chart
