import { I18n } from 'react-i18nify'
import th from "./th.json"
import en from "./en.json"
import jp from "./jp.json"

I18n.setTranslations({
    th,
    en,
    jp,
});

export function getLocale(lang) {

    I18n.setLocale(lang)
    return  I18n
}
