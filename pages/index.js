import React, { useState, useEffect } from 'react';
import socketIOClient from 'socket.io-client'


const Home = () => {

    const [endpoint] = useState('http://localhost:8082');

    const addVote = (framework_id) => {
        const socket = socketIOClient(endpoint)
        socket.emit('addVote', framework_id)
        alert("ขอบคุณสำหรับการโหวต")
      }

    const center = {
        textAlign: "center",
    };
    const setPointer = {
        cursor: "pointer"
    };

    return (
        <div style={center}>
            <div style={setPointer} onClick={() => { addVote(1) }}><img src="../asset/image/an.png" alt="Logo" /></div>
            <div style={setPointer} onClick={() => { addVote(2) }}><img src="../asset/image/react.png" alt="Logo" /></div>
            <div style={setPointer} onClick={() => { addVote(3) }}><img src="../asset/image/vue.png" alt="Logo" /></div>
        </div>
    )
}




export default Home