import React, { useState } from 'react'
import { getLocale } from "../locales/i18n"

const Context = React.createContext({})

function Provider ({ children }) {
    const [lang, setLang] = useState('jp')
    return (
        <Context.Provider value={{
            lang,
            setLang,
            locale: getLocale(lang),
        }}>
            {children}
        </Context.Provider>
    )
}

export default {
    Provider,
    Context,
}